import Model.*;

import java.util.*;

public class Brain {
    Scanner scanner = new Scanner(System.in);
    Scanner scannerHowlong = new Scanner(System.in);

    public void convertInBank(int input, Organization[] organizations) {

        for (Organization organization : organizations) {
            if (organization instanceof Bank) {
                Bank bank1 = (Bank) organization;
                if (input < 12_000) {
                    System.out.println(String.format("Name Bank: %s\nConvert in USD: %.2f\nConvert in EUR: %.2f\nConvert in RUB: %.2f",
                            bank1.getName(),
                            bank1.ExchangeUSD(input),
                            bank1.ExchangeEUR(input),
                            bank1.ExchangeRUB(input)));
                }
            }
        }
    }

    public void CreditingInBank(int input, Organization[] organizations) {

        System.out.println("How much");

        input = Integer.parseInt(scanner.nextLine());
        System.out.println("How long");
        int howLongInput = Integer.parseInt(scannerHowlong.nextLine());
        for (Organization organization : organizations) {
            if (organization instanceof Bank) {
                Bank bank1 = (Bank) organization;
                if (input < 200_000) {
                    float output = bank1.Crediting(input, howLongInput);
                    System.out.println(String.format("Name Bank: %s\nYou paid: %.2f",
                            bank1.getName(),
                            output));
                } else System.out.println("Limit!");
            }
        }
    }

    public void SendMoneyInBank(int input, Organization[] organizations) {
        System.out.println("How much");
        input = Integer.parseInt(scanner.nextLine());
        for (Organization organization : organizations) {
            if (organization instanceof Bank) {
                Bank bank1 = (Bank) organization;
                float output = bank1.SendMoney(input);
                System.out.println(String.format("Name Bank: %s\nYou paid: %.2f",
                        bank1.getName(),
                        output));
            }
        }
    }

    public void ConvertInEchanger(int input, Organization[] organizations) {
        for (Organization organization : organizations) {
            if (organization instanceof Exchanger) {
                Exchanger exchanger = (Exchanger) organization;
                System.out.println(String.format("Name Exchanger: %s\nConvert in USD: %.2f\nConvert in EUR: %.2f\n",
                        exchanger.getName(),
                        exchanger.ExchangeUSD(input),
                        exchanger.ExchangeEUR(input)));
            }
        }
    }

    public void ConvertInLombard(int input, int howLongInput, Organization[] organizations) {
        for (Organization organization : organizations) {
            if (organization instanceof Lombard) {
                Lombard lombard = (Lombard) organization;
                System.out.println(String.format("Name Exchanger: %s\nAfter Model.Credit: %.2f\n",
                        lombard.getName(),
                        lombard.Crediting(input, howLongInput)));
            }
        }
    }

    public void GiveCreditInCreditCafe(int input, Organization[] organizations) {
        System.out.println("How much");

        input = Integer.parseInt(scanner.nextLine());
        System.out.println("How long");
        int howLongInput = Integer.parseInt(scannerHowlong.nextLine());
        for (Organization organization : organizations) {
            if (organization instanceof CreditCafe) {
                CreditCafe creditCafe = (CreditCafe) organization;
                if (input < 4000) {
                    float output = creditCafe.Crediting(input, howLongInput);
                    System.out.println(String.format("Name Bank: %s\nYou paid: %.2f",
                            creditCafe.getName(),
                            output));
                } else System.out.println("Limit!");
            }
        }
    }

    public void GiveCreditInCreditUnite(int input, Organization[] organizations) {
        input = Integer.parseInt(scanner.nextLine());
        System.out.println("How long");
        int howLongInput = Integer.parseInt(scannerHowlong.nextLine());

        for (Organization organization : organizations) {
            if (organization instanceof CreditUnite) {
                CreditUnite creditUnite = (CreditUnite) organization;
                if (input < 100_000) {
                    float output = creditUnite.Crediting(input, howLongInput);
                    System.out.println(String.format("Name Bank: %s\nYou paid: %.2f",
                            creditUnite.getName(),
                            output));
                } else System.out.println("Limit!");
            }
        }
    }

    public void DepositInPIF(int input, Organization[] organizations) {
        System.out.println("How Long");
        if (input > 12) {

        } else System.out.println("Limit!");
    }

    public void SendMoneyInMail(int input, Organization[] organizations) {
        System.out.println("How much");
        input = Integer.parseInt(scanner.nextLine());
        for (Organization organization : organizations) {
            if (organization instanceof Mail) {
                Mail mail = (Mail) organization;
                float output = mail.SendMoney(input);
                System.out.println(String.format("Name Bank: %s\nYou paid: %.2f",
                        mail.getName(),
                        output));
            }
        }
    }

    public void findBestOrgForEchange(Organization[] organizations) {
        int output;
        System.out.println("Enter what you want:" +
                "\n1: Find best USD" +
                "\n2: Работа с обменником" +
                "\n3: Работа с ломбардом");
        int input = Integer.parseInt(scanner.nextLine());

        switch (input) {
            case 1: //Find best USD
                Queue<Float> queue = new PriorityQueue<>();
                Map<String,Float> map = new HashMap<>();
                for (Organization organization : organizations) {
                    if (organization instanceof Bank) {
                        Bank bank = (Bank) organization;
                        map.put(bank.getName(),bank.getCourseUSD());
                        queue.add(bank.getCourseUSD());
                    }
                }
                Arrays.sort(queue.toArray());
//                Arrays.sort(map.values().toArray());

                System.out.println(queue.stream().findFirst());

                break;
            case 2:
                break;
            case 3:
                break;
        }
    }
}
