package Model;

public class Generator {

    public Organization[] generateOrganizations() {
        Organization[] organizations = new Organization[4];

        {
            organizations[0] = new Bank("Privat24", "Plahanovska str", 26.5f, 31f, 4f, 1999,0.25f);
        }
        {
            organizations[1] = new Bank("UkrNacBank", "Maidan str", 27.3f, 31.3f, 3.6f, 1995,0.15f);
        }
        {
            organizations[2] = new Bank("OshadBank", "Pavlovska str", 27f, 30f, 3.5f, 2010,0.20f);
        }
        {
            organizations[3] = new Model.Exchanger("Exchanger1","Plehanovska 11 str",26.3f,29.99f);
        }

        {
            Lombard lombard = new Lombard("Lombard","Pushkinska str",0.4f);
        }
        {
            CreditCafe creditcafe = new CreditCafe("CreditCafe","Onishenka str",2f);
        }
        {
            CreditUnite creditUnite = new CreditUnite("CreditUnite","Grodneva str",0.2f);
        }
        {
            PIF pif = new PIF("PIF","Nutona str",1f);
        }
        {
            Mail mail = new Mail("PIF","Nutona str",1f);
        }

        return organizations;
    }


}
