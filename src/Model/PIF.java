package Model;

public class PIF  extends Organization implements Deposit {
   private float creditProcent;

    public PIF(String name, String adress, float creditProcent) {
        super(name, adress);
        this.creditProcent = creditProcent;
    }


    @Override
    public float Depositing(int uahToDeposit,int howLong) {
        if (howLong >= 12){
            float afterDeposit = uahToDeposit + (uahToDeposit*(creditProcent*howLong));
            return afterDeposit;
        }
        else return 0;
    }
}
