package Model;

public class Exchanger extends Organization implements Banking {

    private float courseUSD;
    private float courseEUR;

    public Exchanger(String name, String adress, float courseUSD, float courseEUR) {
        super(name, adress);
        this.courseUSD = courseUSD;
        this.courseEUR = courseEUR;
    }




    @Override
    public float ExchangeUSD(int uahToExhange) {

        float usd = (uahToExhange / courseUSD);
        if (usd > 0)
            return usd;

        else return 0;
    }

    @Override
    public float ExchangeEUR(int uahToExhange) {

        float eur = (uahToExhange / courseEUR);
        if (eur > 0)
            return eur;
        else return 0;
    }


}
