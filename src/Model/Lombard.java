package Model;

public class Lombard extends Organization implements Credit {
    private int limit = 50_000;
    private float procentYear;

    public Lombard(String name, String adress, float procentYear) {
        super(name, adress);
        this.procentYear = procentYear;
    }

    @Override
    public float Crediting(int uahToConvert, int howLong) {

        if (uahToConvert < limit) {
            float uahAfterCredit = uahToConvert + (uahToConvert * (procentYear * howLong));
            return uahAfterCredit;
        } else {
            return 0;
        }
    }
}
