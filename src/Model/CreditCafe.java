package Model;

public class CreditCafe extends Organization implements Credit {
    private int limit = 4_000;
    private float procentYear;

    public CreditCafe(String name, String adress, float procentYear) {
        super(name, adress);
        this.procentYear = procentYear;
    }

    @Override
    public float Crediting(int uahToConvert, int howLong) {
        if (uahToConvert < limit) {
            float uahAfterCredit = uahToConvert + (uahToConvert * (procentYear * howLong));
            return uahAfterCredit;
        } else {
            return 0;
        }
    }
}