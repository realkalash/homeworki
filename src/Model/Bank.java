package Model;

public class Bank extends Organization implements Banking, Credit, Finance, Deposit {

    private int year;
    private float courseUSD;
    private float courseEUR;
    private float courseRUB;
    private float creditProcent;

    public Bank(String name, String adress, float courseUSD, float courseEUR, float courseRUB, int yearLicense, float creditProcent) {
        super(name, adress);
        this.courseUSD = courseUSD;
        this.courseEUR = courseEUR;
        this.courseRUB = courseRUB;
        this.year = yearLicense;
        this.creditProcent = creditProcent;

    }

    public float getCourseUSD() {
        return courseUSD;
    }

    public float getCourseEUR() {
        return courseEUR;
    }

    public float getCourseRUB() {
        return courseRUB;
    }

    @Override
    public float ExchangeUSD(int uahToExhange) {

        float usd = ((uahToExhange - 15) / courseUSD);
        if (usd > 0)
            return usd;

        else return 0;
    }

    @Override
    public float ExchangeEUR(int uahToExhange) {

        float eur = ((uahToExhange - 15) / courseEUR);
        if (eur > 0)
            return eur;
        else return 0;
    }

    @Override
    public float SendMoney(int uahToSend) {
        float uahAfterSend = (uahToSend + (uahToSend * creditProcent)) + 5;
        if (uahAfterSend > 0)
            return uahAfterSend;
        else return 0;
    }

    @Override
    public String toString() {
        return "Bank: " +
                "uah for convertation: " + 20_000 +
                ", course USD: " + courseUSD +
                ", course EUR: " + courseEUR;
    }

    public float ExchangeRUB(int uahToExchange) {
        float rub = ((uahToExchange - 15) / courseRUB);
        if (rub > 0) {
            return rub;
        } else return 0;
    }

    @Override
    public float Crediting(int uahToConvert, int howLong) {
        float uahAfterCredit = (uahToConvert + (uahToConvert * (creditProcent * howLong) + 5));
        if (uahAfterCredit > 0)
            return uahAfterCredit;
        else return 0;
    }


    @Override
    public float Depositing(int uahToDeposit, int howLong) {
        if (howLong <= 12) {
            float afterDeposit = uahToDeposit + (uahToDeposit * (creditProcent * howLong));
            return afterDeposit;
        } else return 0;
    }
}
