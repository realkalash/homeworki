package Model;

public class Organization {

    private String name;
    private String adress;

    public Organization(String name, String adress) {
        this.name = name;
        this.adress = adress;
    }

    public String getName() {
        return name;
    }

    public String getAdress() {
        return adress;
    }

}
