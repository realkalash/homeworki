package Model;

public class Mail extends Organization implements Finance {
    private float creditProcent;

    public Mail(String name, String adress, float creditProcent) {
        super(name, adress);
        this.creditProcent = creditProcent;
    }

    @Override
    public float SendMoney(int uahToSend) {
        float uahAfterSend = uahToSend + (uahToSend * creditProcent);
        if (uahAfterSend > 0)
            return uahAfterSend;
        else return 0;
    }
}
