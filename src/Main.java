import Model.Generator;
import Model.Organization;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Generator generator = new Generator();
        Brain brain = new Brain();
        Scanner scanner = new Scanner(System.in);
        Scanner scannerHowlong = new Scanner(System.in);
        int input;
        int howLongInput;
        Organization[] organizations = generator.generateOrganizations();

        System.out.println("Enter what you want:" +
                "\n1: Работа с банком" +
                "\n2: Работа с обменником" +
                "\n3: Работа с ломбардом" +
                "\n4: Работа с кредиткафе" +
                "\n5: Работа с кредитным союзом" +
                "\n6: Работа с ПИФ" +
                "\n7: Работа с почтой" +
                "\n8: лучший курс обмена 20.000 грн на доллар/евро/рубль" +
                "\n9: минимальная процентная ставка для кредита на сумму 50.000 грн" +
                "\n10: лучшая организация для пересылки 1000 грн");

        input = Integer.parseInt(scanner.nextLine());
        switch (input) {
            case 1:         // БАНК
                System.out.println("Работа с банком:" +
                        "\n1: менять валюту (доллар, евро, рубль) Максимальная сумма обмена 12.000 грн. Комиссия - 15 грн." +
                        "\n2: взять денег в долг под 20% годовых. Максимум 200 000" +
                        "\n3: Переслать деньги 1% от суммы + 5 грн" +
                        "\n4: Депозит до 12 месяцев");


                input = Integer.parseInt(scanner.nextLine());

                System.out.println("Введите кол-во грн для конвертации в банке");
                switch (input) {
                    case 1:     //CONVERT
                        input = Integer.parseInt(scanner.nextLine());
                        brain.convertInBank(input, organizations);
                        break;
                    case 2:     //CREDITING
                        brain.CreditingInBank(input, organizations);
                    case 3:     //SENDMONEY
                        brain.SendMoneyInBank(input, organizations);
                }
                break;


            case 2:         // ОБМЕННИК
                System.out.println("Введите кол-во грн для конвертации в Обменнике");

                input = Integer.parseInt(scanner.nextLine());
                brain.ConvertInEchanger(input, organizations);

                break;
            case 3:         // ЛОМБАРД
                System.out.println("Введите кол-во грн для конвертации в Ломбарде");
                input = Integer.parseInt(scanner.nextLine());


                System.out.println("На сколько месяцев?");
                howLongInput = Integer.parseInt(scanner.nextLine());

                brain.ConvertInLombard(input, howLongInput, organizations);

                break;
            case 4:         //КРЕДИТКАФЕ
                System.out.println("Дать денег в долг в КредитКафе");
                System.out.println("How much");

                input = Integer.parseInt(scanner.nextLine());

                brain.GiveCreditInCreditCafe(input, organizations);
                break;
            case 5:     //КРЕДИТНЫЙ СОЮЗ
                System.out.println("Дать денег в долг в Кредитном союз");
                System.out.println("How much");
                input = Integer.parseInt(scanner.nextLine());
                brain.GiveCreditInCreditUnite(input, organizations);

                break;
            case 6:     //ПИФ
                System.out.println("Депозит в ПИФ");
                System.out.println("How much");
                input = Integer.parseInt(scanner.nextLine());

                brain.DepositInPIF(input, organizations);

                break;
            case 7:     //ПОЧТА
                System.out.println("Пересылать деньги по почте");
                System.out.println("How much");
                input = Integer.parseInt(scanner.nextLine());

                brain.SendMoneyInMail(input, organizations);
                break;
            case 8:     //лучший курс обмена 20.000 грн на доллар/евро/рубль
                System.out.println("лучший курс обмена 20.000 грн на доллар/евро/рубль");

                brain.findBestOrgForEchange(organizations);
                break;
        }
    }


}